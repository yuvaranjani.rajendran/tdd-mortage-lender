import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


final class LenderTest {
    //team 9
    @Test
    void canCreateLender(){
        Lender lender = new Lender("1", 1000);
        assertNotNull(lender);
    }
    @Test
    void canCheckAvailableBalance(){
        Lender lender = new Lender("1", 1000);
        assertEquals(1000, lender.getAvailableFunds());
    }

}
