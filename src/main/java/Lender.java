final class Lender {

    private String lenderId;
    private int availableFunds;

    public Lender(String lenderId, int availableFunds) {
        this.lenderId = lenderId;
        this.availableFunds = availableFunds;
    }

    public int getAvailableFunds() {
        return availableFunds;
    }
}
